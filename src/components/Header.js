import React from "react";
import { Link } from "react-router-dom";
import Logo from "../assets/logo.png";
import styled from "styled-components";

function Header() {
    return (
        <HeaderContainer>
            <TopLeft>
                <LogoContainer>
                    <Top href="#">
                        <Image src={Logo} alt="Logo" />
                    </Top>
                </LogoContainer>
                <Menu to="to">
                    <MenuIcon
                        src={require("../assets/Group 669.png")}
                        alt="Image"
                    />
                </Menu>
                <Form>
                    <SearchIcon
                        src={require("../assets/Layer -4.png")}
                        alt="Image"
                    />
                    <Input type="text" placeholder="search" />
                </Form>
            </TopLeft>
            <TopRight>
                <Notification to="#">
                    <NotificationIcon
                        src={require("../assets/Layer -5.png")}
                        alt="Image"
                    />
                </Notification>
                <Message to="#">
                    <MessageIcon
                        src={require("../assets/Layer -6.png")}
                        alt="Image"
                    />
                </Message>
                <User>
                    <Person
                        src={require("../assets/Ellipse 9.png")}
                        alt="Image"
                    />
                </User>
                <Text>lorem Ipsum</Text>
                <Arrow src={require("../assets/Polygon 1.png")} alt="Image" />
            </TopRight>
        </HeaderContainer>
    );
}
const HeaderContainer = styled.div`
    background-color: #1f2935;
    height: 82px;
    display: flex;
    justify-content: space-between;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    padding-right: 35px;
`;
const LogoContainer = styled.h1`
    margin-right: 100px;
`;
const Top = styled(Link)`
    display: inline-block;
`;
const Image = styled.img``;
const Menu = styled(Link)`
    display: inline-block;
`;
const MenuIcon = styled.img`
    margin-right: 30px;
    display: block;
`;
const Form = styled.form`
    display: flex;
    background-color: #15232e;
    padding: 10px;
    align-items: center;
    width: 385px;
`;
const Input = styled.input`
    background-color: #15232e;
    color: #fff;
    border: none;
`;
const SearchIcon = styled.img`
    display: block;
    margin-right: 20px;
`;
const TopLeft = styled.div`
    display: flex;
    align-items: center;
    padding-left: 35px;
`;
const TopRight = styled.div`
    display: flex;
    align-items: center;
    padding-right: 35px;
`;
const Notification = styled(Link)`
    margin-right: 25px;
`;
const NotificationIcon = styled.img`
    display: block;
    width: 100%;
`;
const Message = styled(Link)`
    margin-right: 25px;
`;
const MessageIcon = styled.img`
    display: block;
    width: 100%;
`;
const User = styled.div`
    margin-right: 25px;
`;
const Person = styled.img`
    display: block;
    width: 100%;
`;
const Text = styled.span`
    color: #fff;
`;
const Arrow = styled.img`
    display: block;
`;

export default Header;
