import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

function NavBar() {
    return (
        <>
            <Nav>
                <Aside>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Layer 2.png")}
                                alt="Image"
                            />
                            <Text>dashboard</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Layer -1.png")}
                                alt="Image"
                            />
                            <Text>ticket</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Group 9.png")}
                                alt="Image"
                            />
                            <Text>support</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Layer -2.png")}
                                alt="Image"
                            />
                            <Text>claim</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Group 11.png")}
                                alt="Image"
                            />
                            <Text>report</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Layer -3.png")}
                                alt="Image"
                            />
                            <Text>job desks</Text>
                        </Item>
                    </List>
                    <List>
                        <Item to="#">
                            <Image
                                src={require("../assets/Group 10.png")}
                                alt="Image"
                            />
                            <Text>my students</Text>
                        </Item>
                    </List>
                </Aside>
            </Nav>
        </>
    );
}

const Nav = styled.div`
    background-color: #1f2935;
    width: 260px;
    height: 100vh;
    position: fixed;
    top: 82px;
    left: 0;
`;
const Aside = styled.ul`
    padding: 0;
    display: flex;
    flex-direction: column;
`;
const List = styled.li`
    padding: 20px 20px;
`;
const Item = styled(Link)`
    display: flex;
    align-items: center;
`;
const Image = styled.img`
    margin-right: 20px;
`;
const Text = styled.span`
    color: #fff;
    text-transform: capitalize;
`;

export default NavBar;
