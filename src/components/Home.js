import React from "react";
import NavBar from "./NavBar.js";
import Header from "./Header.js";
import Main from "./Main.js";
import styled from "styled-components";

function Home() {
    return (
        <HomeContainer>
            <Header />
            <NavBar />
            <Main />
        </HomeContainer>
    );
}

const HomeContainer = styled.div``;

export default Home;
