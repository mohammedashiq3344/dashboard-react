import React from "react";
import styled from "styled-components";
import Card from "./Card";

function Main() {
    return (
        <>
            <MainContainer>
                <Top>
                    <Heading>students</Heading>
                    <Form>
                        <SearchIcon
                            src={require("../assets/Layer -4.png")}
                            alt="Image"
                        />
                        <Input type="text" placeholder="search" />
                    </Form>
                </Top>
                <Card />
            </MainContainer>
        </>
    );
}
const MainContainer = styled.div`
    background-color: #15232e;
    height: 100vh;
    margin-left: 260px;
    margin-top: 82px;
    padding: 30px 30px 0 30px;
`;
const Top = styled.div`
    background-color: #1f2935;
    display: flex;
    justify-content: space-between;
    padding: 25px;
`;
const Heading = styled.h1`
    color: #ffffff;
    margin: 0;
`;
const Form = styled.form`
    display: flex;
    background-color: #15232e;
    padding: 10px;
    align-items: center;
    width: 385px;
`;
const Input = styled.input`
    background-color: #15232e;
    color: #fff;
    border: none;
`;
const SearchIcon = styled.img`
    display: block;
    margin-right: 20px;
`;

export default Main;
