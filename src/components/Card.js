import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

function Card() {
    return (
        <Box>
            <List>
                <ListTop>
                    <User>
                        <Person
                            src={require("../assets/Ellipse 9.png")}
                            alt="Image"
                        />
                    </User>
                    <Text>
                        <Name>Bessie Richards</Name>
                        <Star>7 star progile</Star>
                    </Text>
                </ListTop>
                <ListMiddle>
                    <EmailContainer>
                        <Message
                            src={require("../assets/Layer -6.png")}
                            alt="Image"
                        />
                        <Email to="#">example@sample.com</Email>
                    </EmailContainer>
                    <ContactContainer>
                        <Phone
                            src={require("../assets/Layer -3.png")}
                            alt="Image"
                        />
                        <Contact to="#">+91 4585 125 152</Contact>
                    </ContactContainer>
                </ListMiddle>
                <ListBottom>
                    <Heading>ui engineer</Heading>
                    <ButtonContainer1>
                        <Button1 to="#">ui engineer</Button1>
                        <Button2 to="#">ui engineer</Button2>
                    </ButtonContainer1>
                    <ButtonContainer2>
                        <MessageButton to="#">
                            <MessageImage
                                src={require("../assets/Layer -6.png")}
                                alt="Image"
                            />
                            <MessageText>Message</MessageText>
                        </MessageButton>
                        <CallButton to="#">
                            <PhoneImage
                                src={require("../assets/Layer -3.png")}
                                alt="Image"
                            />
                        </CallButton>
                        <CallText>Call</CallText>
                    </ButtonContainer2>
                </ListBottom>
            </List>
        </Box>
    );
}

const Box = styled.ul`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    list-style: none;
    background-color: #1f2935;
`;
const List = styled.li`
    background-color: #15232e; ;
`;
const ListTop = styled.div``;
const User = styled.div``;
const Person = styled.img``;
const Text = styled.div``;
const Name = styled.h4``;
const Star = styled.span``;
const ListMiddle = styled.div``;
const EmailContainer = styled.div``;
const Message = styled.img``;
const Email = styled(Link)``;
const ContactContainer = styled.div``;
const Phone = styled.img``;
const Contact = styled(Link)``;
const ListBottom = styled.div``;
const Heading = styled.h1``;
const ButtonContainer1 = styled.div``;
const Button1 = styled.button``;
const Button2 = styled.button``;
const ButtonContainer2 = styled.div``;
const MessageButton = styled.button``;
const MessageImage = styled.img``;
const MessageText = styled.span``;
const CallButton = styled.button``;
const PhoneImage = styled.img``;
const CallText = styled.span``;

export default Card;
